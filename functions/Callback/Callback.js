function greet(name) {
    console.log(`Hello, ${name}!`);
}

function sayHello(callback) {
    const name = 'John';
    callback(name);
}

sayHello(greet);


function square(num) {
    return num * num;
}

function calculate(callback) {
    const number = 5;
    const result = callback(number);
    console.log(result);
}

calculate(square);
