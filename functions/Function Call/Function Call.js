function greet() {
    console.log(`Hello, ${this.name}!`);
}

const person = {
    name: 'John'
};

greet.call(person);

function calculateSum(a, b) {
    console.log(a + b);
}

calculateSum.call(null, 4, 7);
