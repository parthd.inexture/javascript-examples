function greet() {
    console.log('Hello after 3 seconds!');
}

setTimeout(greet, 3000);

function displayMessage() {
    console.log('This message will be displayed after 5 seconds.');
}

const timeoutId = setTimeout(displayMessage, 5000);
clearTimeout(timeoutId);
