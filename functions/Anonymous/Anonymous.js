const multiply = function (a, b) {
    return a * b;
};

const result = multiply(4, 5);
console.log(result);


const sayHello = function (name) {
    console.log(`Hello, ${name}!`);
};

sayHello('Jane');
