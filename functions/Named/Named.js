function addNumbers(a, b) {
    return a + b;
}

const result = addNumbers(5, 3);
console.log(result);

function greet(name) {
    console.log(`Hello, ${name}!`);
}

greet('John');
