function outerFunction() {
    const message = 'Hello';

    function innerFunction() {
        console.log(message);
    }

    return innerFunction;
}

const myFunction = outerFunction();
myFunction();

function counter() {
    let count = 0;

    function increment() {
        count++;
        console.log(count);
    }

    return increment;
}

const incrementCounter = counter();
incrementCounter();
incrementCounter();
incrementCounter();
