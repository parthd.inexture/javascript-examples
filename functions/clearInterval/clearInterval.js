let count = 0;

function increment() {
    count++;
    console.log(count);
}

const intervalId1 = setInterval(increment, 1000);

setTimeout(function () {
    clearInterval(intervalId1);
}, 5000);


let seconds = 0;

function incrementSeconds() {
    seconds++;
    console.log(`Elapsed time: ${seconds} seconds.`);
}

const intervalId2 = setInterval(incrementSeconds, 1000);

setTimeout(function () {
    clearInterval(intervalId2);
}, 10000);
