function greet() {
    console.log(`Hello, ${this.name}!`);
}

const person = {
    name: 'John'
};

const greetPerson = greet.bind(person);
greetPerson();

function calculateSum(a, b) {
    console.log(a + b);
}

const addNumbers = calculateSum.bind(null, 4, 7);
addNumbers();
