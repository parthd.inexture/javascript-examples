function greet(name) {
    const message = `Hello, ${name}!`;
    console.log(message);
}

const personName = 'John';
greet(personName);
