function greet() {
    console.log(`Hello, ${this.name}!`);
}

const person = {
    name: 'John'
};

greet.apply(person);

function calculateSum(a, b) {
    console.log(a + b);
}

calculateSum.apply(null, [4, 7]);
