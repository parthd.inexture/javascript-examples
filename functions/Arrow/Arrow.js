const square = (num) => num * num;

const result = square(3);
console.log(result);


const greet = (name) => console.log(`Hello, ${name}!`);

greet('Alice');
