function toggleVisibility() {
    const myDiv = document.getElementById('myDiv');
    myDiv.style.display = myDiv.style.display === 'none' ? 'block' : 'none';
}

function toggleClass() {
    const myDiv = document.getElementById('myDiv');
    myDiv.classList.toggle('highlight');
}