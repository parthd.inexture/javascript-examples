const myDiv = document.getElementById('root');
const newParagraph = document.createElement('p');
newParagraph.textContent = 'New paragraph';
myDiv.appendChild(newParagraph);

function removeListItem() {
    const myList = document.getElementById('myList');
    const firstItem = myList.querySelector('li');
    myList.removeChild(firstItem);
}