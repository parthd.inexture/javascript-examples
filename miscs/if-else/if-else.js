const age = 18;

if (age >= 18) {
    console.log('You are eligible to vote');
} else {
    console.log('You are not eligible to vote');
}


const temperature = 25;

if (temperature > 30) {
    console.log('It is hot outside');
} else if (temperature > 20) {
    console.log('It is warm outside');
} else {
    console.log('It is cool outside');
}
