const numbers = [1, 2, 3, 4, 5];

numbers.forEach(function (num) {
    console.log(num);
});


const names = ['Alice', 'Bob', 'Charlie'];

names.forEach(function (name, index) {
    console.log(`Name ${index + 1}: ${name}`);
});
