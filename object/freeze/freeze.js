const person = {
    name: 'John',
    age: 30
};

Object.freeze(person);

person.name = 'Bob';
console.log(person.name);


const car = {
    brand: 'Toyota',
    model: 'Camry'
};

Object.freeze(car);

car.model = 'Corolla';
console.log(car.model);
