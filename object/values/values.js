const person = {
    name: 'John',
    age: 30,
    city: 'New York'
};

const values1 = Object.values(person);
console.log(values1);


const car = {
    brand: 'Toyota',
    model: 'Camry',
    year: 2022
};

const values2 = Object.values(car);
console.log(values2);
