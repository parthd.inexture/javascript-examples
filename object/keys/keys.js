const person = {
    name: 'John',
    age: 30,
    city: 'New York'
};

const personKeys = Object.keys(person);
console.log(personKeys);

const car = {
    brand: 'Toyota',
    model: 'Camry',
    year: 2022
};

const carKeys = Object.keys(car);
console.log(carKeys);