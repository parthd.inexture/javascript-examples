const person = {
    name: 'John',
    age: 30,
    city: 'New York'
};

const entries1 = Object.entries(person);
console.log(entries1);


const car = {
    brand: 'Toyota',
    model: 'Camry',
    year: 2022
};

const entries2 = Object.entries(car);
console.log(entries2);
