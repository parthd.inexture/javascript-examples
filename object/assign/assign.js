const obj1 = { a: 1, b: 2 };
const obj2 = { c: 3, d: 4 };

const mergedObj1 = Object.assign({}, obj1, obj2);
console.log(mergedObj1);

const user = { name: 'John', age: 30 };
const address = { city: 'New York', country: 'USA' };

const mergedObj2 = Object.assign({}, user, address);
console.log(mergedObj2);
