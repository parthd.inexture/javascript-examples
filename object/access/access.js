const person = {
    fname: "Parth",
    lname: "Desai",
    age: 25
};

console.log('My name is ' + person.fname + " " + person.lname + " and my age is " + person.age);
console.log('My name is ' + person['fname'] + " " + person['lname'] + " and my age is " + person['age']);