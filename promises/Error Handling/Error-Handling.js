const fetchUserData = (userId) => {
    return new Promise((resolve, reject) => {

        setTimeout(() => {
            const userData = { id: userId, name: 'John' };

            if (userData) {
                resolve(userData);
            } else {
                reject(new Error('User data not found'));
            }
        }, 2000);
    });
};

const getUserData = async () => {
    try {
        const user = await fetchUserData(123);
        console.log('User data:', user);
    } catch (error) {
        console.log('Error:', error.message);
    }
};

getUserData();

// Smaple 2

const fetchImageData = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const image = { name: 'Image 1', url: 'https://example.com/image.jpg' };

            if (image) {
                resolve(image);
            } else {
                reject(new Error('Failed to fetch image data'));
            }
        }, 3000);
    });
};

const displayImage = async () => {
    try {
        const image = await fetchImageData();
        console.log('Image:', image);
    } catch (error) {
        console.log('Error:', error.message);
    }
};

displayImage();
