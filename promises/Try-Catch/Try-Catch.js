const divide = (a, b) => {
    try {
        if (b === 0) {
            throw new Error('Divide by zero error');
        }
        return a / b;
    } catch (error) {
        console.log('Error:', error.message);
    }
};

console.log(divide(10, 2));
console.log(divide(8, 0));

// Sample 2

const readDataFromFile = () => {
    try {

        const data = 'This is some data';
        if (data) {
            console.log('Data:', data);
        } else {
            throw new Error('Failed to read data from file');
        }
    } catch (error) {
        console.log('Error:', error.message);
    }
};

readDataFromFile();
