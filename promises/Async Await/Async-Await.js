const fetchUserData = (userId) => {
    return new Promise((resolve, reject) => {

        setTimeout(() => {
            const userData = { id: userId, name: 'John' };

            if (userData) {
                resolve(userData);
            } else {
                reject('User data not found');
            }
        }, 2000);
    });
};

const getUserData = async () => {
    try {
        const user = await fetchUserData(123);
        console.log('User data:', user);
    } catch (error) {
        console.log('Error:', error);
    }
};

getUserData();


// Sample 2

const fetchPosts = () => {
    return new Promise((resolve, reject) => {

        setTimeout(() => {
            const posts = ['Post 1', 'Post 2', 'Post 3'];

            if (posts) {
                resolve(posts);
            } else {
                reject('Failed to fetch posts');
            }
        }, 3000);
    });
};

const displayPosts = async () => {
    try {
        const posts = await fetchPosts();
        console.log('Posts:', posts);
    } catch (error) {
        console.log('Error:', error);
    }
};

displayPosts();
