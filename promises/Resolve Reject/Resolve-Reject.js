const fetchUserData = (userId) => {
    return new Promise((resolve, reject) => {

        setTimeout(() => {
            const userData = { id: userId, name: 'John' };

            if (userData) {
                resolve(userData);
            } else {
                reject('User data not found');
            }
        }, 2000);
    });
};

fetchUserData(123)
    .then((user) => {
        console.log('User data:', user);
    })
    .catch((error) => {
        console.log('Error:', error);
    });


// Sample 2
const fetchWeatherData = () => {
    return new Promise((resolve, reject) => {

        setTimeout(() => {
            const weatherData = { temperature: 25, condition: 'Sunny' };

            if (weatherData) {
                resolve(weatherData);
            } else {
                reject('Weather data not available');
            }
        }, 3000);
    });
};

fetchWeatherData()
    .then((weather) => {
        console.log('Weather data:', weather);
    })
    .catch((error) => {
        console.log('Error:', error);
    });
