// const currentMethod = 'concat';

// const public_users = ['John', "Nick"];
// const pricate_users = ['Dohn', "Farhan"];
// const users = public_users.concat(pricate_users);

// function ex1() {
//     const array1 = [1, 2];
//     const array2 = [3, 4];
//     const newArray = array1.concat(array2);

//     return `Arr1: ${array1} | Arr2: ${array1} | New Array: ${newArray}`;
// }


// function ex2() {
//     const array1 = ['a', 'b'];
//     const array2 = ['c', 'd'];
//     const newArray = array1.concat(array2);

//     return `Arr1: ${array1} | Arr2: ${array1} | New Array: ${newArray}`;
// }

// function concatFunction() {
//     const currentElement = document.getElementById(currentMethod);

//     console.log(currentElement);

//     let content = "";
//     content += "<br>" + ex1();
//     content += "<br>" + ex2();
//     currentElement.innerHTML = content;
// }

// document.addEventListener("DOMContentLoaded", () => {
//     const rootElement = document.getElementById('root');

//     const element = `
//         <div>
//             <button onclick="${currentMethod}Function()">Perform ${currentMethod} examples</button>
//             <div id="${currentMethod}"></div>
//             <ul> </ul>
//         </div>
//     `;

//     console.log(element);


//     rootElement.innerHTML = rootElement.innerHTML + element;

// });

let array1 = [1, 2];
let array2 = [3, 4];
let newArray = array1.concat(array2);
console.log(newArray); 


array1 = ['a', 'b'];
array2 = ['c', 'd'];
newArray = array1.concat(array2);
console.log(newArray); 
