const nestedArray1 = [1, [2, 3], 4, [5]];
const flattenedArray1 = nestedArray1.flat();
console.log(flattenedArray1);


const nestedArray2 = ['a', ['b', ['c']], 'd'];
const flattenedArray2 = nestedArray2.flat();
console.log(flattenedArray2);

