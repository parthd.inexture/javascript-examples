const numbers = [1, 2, 3, 4, 5];
const removedElements = numbers.splice(1, 2);
console.log(removedElements); 
console.log(numbers); 


const fruits = ['apple', 'banana', 'orange', 'kiwi'];
const removedFruits = fruits.splice(2, 2);
console.log(removedFruits); 
console.log(fruits); 
