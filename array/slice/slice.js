const numbers = [1, 2, 3, 4, 5];
const slicedArray1 = numbers.slice(1, 4);
console.log(slicedArray1); 

const colors = ['red', 'green', 'blue', 'yellow', 'orange'];
const slicedArray2 = colors.slice(2);
console.log(slicedArray2); 
